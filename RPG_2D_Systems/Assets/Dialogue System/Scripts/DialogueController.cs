using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueController : MonoBehaviour
{
    [Header("DialogBox UI")]
     public GameObject dialogBox;
     public TextMeshProUGUI characterName;
     public Image characterDisplay;
     public TextMeshProUGUI dialogHolder;
     public float typingSpeed;
    //interate

    /*
        iterate dialog
        check option
    */

    IEnumerator writeDialog(string sentence){
        char[] letters = sentence.ToCharArray();
        foreach(char c in letters){
            dialogHolder.text += c;
            yield return new WaitForSeconds(typingSpeed);
        }
    }

    public void playDialog(DialogueItem di){
        int counter = 0;
        foreach(string sentence in di.dialogues){
            characterName.text = di.characterName;
            characterDisplay.sprite = di.characterSprite[counter];
            StartCoroutine(writeDialog(sentence));
            counter += 1;
        }
    }

    
}
