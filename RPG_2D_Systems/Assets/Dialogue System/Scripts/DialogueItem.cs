using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Dialogue", menuName = "STORY/DialogueItem", order = 0)]
public class DialogueItem : ScriptableObject
{
    //charname
    public string characterName;
    //charsprite
    public List<Sprite> characterSprite;
    //
    public List<string> dialogues;
    
    public enum option{
        QUEST,
        QUESTION,
        DEFAULT
    }

    public option dialogOption;

    public List<string> responses;
    
}
