using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class AudioController : Singleton<AudioController>
{
    
    public SoundStat[] sounds;
	public static bool BGsoundPlaying = false;

	
	private void Start() {
         DontDestroyOnLoad(gameObject);
	}
	 protected override void Awake() {
         base.Awake();
         foreach(SoundStat s in sounds)
			{
				s.source = gameObject.AddComponent<AudioSource>();
				s.source.clip = s.clip;
				s.source.volume = s.volume;
				s.source.pitch = s.pitch;
				s.source.loop = s.loop;
			}
			
     }
    
    public void PlaySound(string soundName){
        SoundStat s = Array.Find (sounds, sound => sound.nameClip == soundName);
		if (s == null) {
			Debug.LogWarning ("Sound: " + soundName + " not found");
			return;
		}
		if(s.source.isPlaying){
			return;
		}else{
			s.source.Play ();
		}
		
    }

	public void StopSound(string soundName){
		SoundStat s = Array.Find(sounds, sound => sound.nameClip == soundName);

		if (s == null) {
			Debug.LogWarning ("Sound: " + soundName+ " not found");
			return;
		}
		s.source.Stop();
	}

	public void StopCurrentPlaying(){
		foreach(SoundStat s in sounds){
			if(s.source.isPlaying){
				StopSound(s.nameClip);
			}
		}
	}

	public void PauseSound(string soundName){
		SoundStat s = Array.Find(sounds, sound => sound.nameClip == soundName);

		if (s == null) {
			Debug.LogWarning ("Sound: " + soundName + " not found");
			return;
		}
		s.source.Pause();
	}

	public void sfxVolumeChanged(GameObject slider){
		float volume = slider.GetComponent<Slider>().value;
		foreach(SoundStat s in sounds){
			if(s.soundType == SoundStat.SoundType.SFX){
				s.source.volume = volume;
			}
		}
	}

}